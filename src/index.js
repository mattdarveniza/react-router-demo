import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom'


import './index.css';
import App from './App';

const INITIAL_STATE = {
  shifts: [
    { id: 1, taskIds: [1, 2] },
    { id: 2, taskIds: [3] },
    { id: 3, taskIds: [] },
    { id: 4, taskIds: [] },
  ],
  tasks: [
    { id: 1 },
    { id: 2 },
    { id: 3 },
  ]
}

const rootReducer = function (state = INITIAL_STATE, action) {
  return state
}

const store = createStore(rootReducer)

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <>
        <nav>Navbar</nav>
        <Route path="/" component={App} />
      </>
    </Provider>
  </Router>,
  document.getElementById('root'),
);
