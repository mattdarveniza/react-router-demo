import React from 'react';
import { NavLink, Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';

const TaskItem = ( {taskId, selectedTaskId } ) => (
  <>Task {taskId} {taskId === parseInt(selectedTaskId, 10) && 'Selected'}</>
)

const ShiftsComp = ({ shifts, match }) => (
  <ul>
    {shifts.map(shift => (
      <li>
        <NavLink exact to={`/shifts/${shift.id}`}>Shift {shift.id}</NavLink>
        <ul>
          {shift.taskIds.map(taskId => (
            <li>
              <NavLink exact to={`/shifts/${shift.id}/tasks/${taskId}`}>
                <Route
                  path="/shifts/:shiftId/tasks/:taskId"
                  children={({ match }) => (
                    <TaskItem taskId={taskId} selectedTaskId={match && match.params.taskId} />
                  )}>
                </Route>
              </NavLink>
            </li>
          ))}
        </ul>
      </li>
    ))}
  </ul>
)

const Shifts = withRouter(connect((state, { match }) => ({
  shifts: state.shifts,
  match,
}))(ShiftsComp))

const ShiftDetailComp = ({ shift }) => (
  <>
    <div>Shift {shift.id} selected</div>
    <Route exact path={`/shifts/:shiftId/tasks/:taskId`} component={TaskDetail} />
  </>
)

const ShiftDetail = connect((state, { match }) => ({
  shift: state.shifts.find(s => s.id === parseInt(match.params.shiftId, 10))
}))(ShiftDetailComp)

const TasksDetailComp = ({ task }) => <div>Task {task.id} selected</div>

const TaskDetail = connect((state, { match }) => ({
  task: state.tasks.find(t => t.id === parseInt(match.params.taskId), 10)
}))(TasksDetailComp)

const NoDetail = () => <div>Select something!</div>

class App extends React.PureComponent {
  render() {
    return (
      <div className="App">
        <Shifts />
        <Switch>
          <Route path={`/shifts/:shiftId`} component={ShiftDetail} />
          <Route component={NoDetail} />
        </Switch>
      </div>
    );
  }
}

export default App;
